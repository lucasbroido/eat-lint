class Lint {
  
  constructor(image) {
    this.n = 500;
    this.particles = new Array();
    this.size =7;
    this.count = this.n;
    this.img = image;
    this.img.resize(windowWidth, windowHeight);
    this.linted = 0;
    for (var i = 0; i<this.n; i++) {
      this.generate();  
    }
  }
  
  generate() {
    var d = 80;
    var dx = random(-d,d);
    var dy = random(-d,d);
    let x = floor(random(0,this.img.width));
    let y = floor(random(0,this.img.height));

    var c = color(this.img.get(x,y));
    let p = new Particle(x+dx,y+dy,this.size,c);
    this.particles.push(p);
    var length = random(0,20);
    var r = 4;
    for (var i = 0; i<length; i++) {
      var newx = x + random(-r,r);
      var newy = y + random(-r,r);
      c = color(this.img.get(newx,newy));
      c.setAlpha(random(0,150));
      var pa = new Particle(newx+dx, newy+dy, this.size,c);
      this.particles.push(pa);
      this.count++;
    }
    
  }
  
  delint() {
    let suckers = new Array();
    for (var i = 0; i<this.particles.length; i++) {
      var d = int(dist(this.particles[i].x, this.particles[i].y, mouseX, mouseY));      
      if (d < 50) {
        if (!this.particles[i].linted) {        
          suckers.push(this.particles[i]);
          this.particles[i].alpha = 0;
          this.particles[i].colour.setAlpha(this.particles[i].alpha);
        }
      }
    }
    this.reduce(suckers);
  }

  reduce(suckers) {

    for (var i = 0; i < suckers.length; i++) {
      suckers[i].linted = true;
      this.linted++;
      suckers.pop(suckers[i]);

    }

  }
  
  update() {
    for (var i = 0; i<this.particles.length; i++) {
      fill(this.particles[i].colour);
      circle(this.particles[i].x, this.particles[i].y, this.size);
    }
  }
  
}