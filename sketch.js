function preload() {
  this.img = loadImage('knitted.jpg');
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  this.lint = new Lint(this.img);
  this.linter = new Linter(this.lint);

}

function draw() {
  // background(255);
  image(this.img, 0, 0,windowWidth,windowHeight);
  this.img.resize(windowWidth, 0);

  this.lint.update();
  this.linter.display();
  if (mouseIsPressed) {
      this.lint.delint();

  }
}

function returnImage() {
  return this.img;
}

