class Linter {
  
  constructor(lint) {
    this.x;
    this.y;
    this.r = 50;
    this.lint = lint;
    this.percentfull = 0;
    this.lp = new Array();
    }
  
  
  display() {

    if (mouseIsPressed) {
      this.x = mouseX;
      this.y = mouseY;
      stroke(0);
      fill(120);
      circle(mouseX+this.r/4, mouseY-4, this.r*2);

      stroke(0);
      fill(148,0,211);
      rect(mouseX-this.r/2,mouseY-this.r, this.r*3/2, this.r*4,20);
      console.log("count: ",this.lint.count," linted: ",this.lint.linted, " percent: ",round(100*this.percentfull));
      fill(167,166,186);
      // stroke(0);
      rect(mouseX-6,mouseY-this.r/2, this.r*3/4, this.r*3,5);
      strokeWeight(10);
      
      this.generateLint(mouseX-6,mouseY-this.r/2, mouseX-4 + this.r*3/4, mouseY-this.r/2+this.r*3);
      //line(mouseX-this.r/4, mouseY+this.r*3*(1-this.percentfull), mouseX+this.r/4, mouseY+this.r*3*(1-this.percentfull));
      strokeWeight(1);
      noStroke();
      if (this.lint) {
        this.percentfull = this.lint.linted / this.lint.count;
      }
    }

    else {
      this.x = mouseX;
      this.y = mouseY;
      stroke(0);
      fill(120);
      rect(mouseX-this.r,mouseY-this.r,this.r, this.r*2,20)
      //stroke(255);
      fill(148,0,211);
      rect(mouseX-this.r/2-5,mouseY-this.r, this.r+5, this.r*4,5);
      fill(255);
      rect(mouseX-this.r/3,mouseY-this.r/2, this.r/3-5, this.r*2,5);
      rect(mouseX+this.r/4-10,mouseY+this.r/2, this.r/3-5, this.r*2,5);
      noStroke();
    }
  }

  generateLint(x1,y1,x2,y2) {
    var n = round(this.percentfull*this.lint.count/12)
    var size = 6;
    var dx = 5;
    var dy = 5;

    for (var i=0; i < n; i++) {
      var rx = random(x1+dx/2,x2-dx/2);
      var ry = random(y1+dy/2,y2-dy/2);
      var c = color(70,70,70);
      var p = new Particle(rx,ry,size,c,50)
      this.lp.push(p);
    }


  }
  
}
  