class Particle {
  
  constructor(x,y,size,colour, alpha = 255) {

    this.colour = colour;
    this.x = x;
    this.y = y;
    this.size = size;
    this.appear();
    this.alpha = alpha;
    
    
    this.linted = false;
  }
  
  
  appear() {
    noStroke();
    fill(this.colour);
    circle(this.x,this.y,this.size);    
  }
  
  
}